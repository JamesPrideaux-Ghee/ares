The FUSE subsystem is made available through the includes
libLSS/tools/fused_array.hpp, libLSS/tools/fuse_wrapper.hpp. They define
wrappers and operators to make the writing of expressions on array
relatively trivial, parallelized and possibly vectorized if the arrays
permit. To illustrate this there are two examples in the library of
testcases: test_fused_array.cpp and test_fuse_wrapper.cpp.

We will start from a most basic example:

::

     boost::multi_array<double, 1> a(boost::extents[N]);
     auto w_a = LibLSS::fwrap(a);

     w_a = 1;

These few lines create a one dimensional array of length N. Then this
array is wrapped in the seamless FUSE expression system. It is quite
advised to use auto here as the types can be complex and difficult to
guess for newcomers. Finally, the last line fills the array with value
1. This is a trivial example but we can do better:

::

     w_a = std::pow(std::cos(w_a*2*M_PI), 2);

This transforms the content of a by evaluating :math:`cos(2\pi x)^2` for
each element :math:`x` of the array wrapped in w_a. This is done without
copy using the lazy expression mechanism. It is possiible to save the
expression for later:

::

     auto b = std::pow(std::cos(w_a*2*M_PI), 2);

Note that nothing is evaluated. This only occurs at the assignment
phase. This wrap behaves also mostly like a virtual array:

.. raw:: html

   </pre>

`` (*b)[i]``

.. raw:: html

   </pre>

accesses computes the i-th value of the expression and nothing else.

Some other helpers in the libLSS supports natively the fuse mechanism.
That is the case for ``RandomNumber::poisson`` for example:

::

     auto c = fwrap(...);
     c = rgen.poisson(b);

This piece of code would compute a poisson realization for a mean value
given by the element of the ``b`` expression (which must be a wrapped
array or one expression of it) and stores this into ``c``.

The ``sum`` reduce (parallel reduction) operation is supported by the
wrapper:

::

     double s = c.sum();

Some arrays could be entirely virtual, i.e. derived from C++
expressions. This needs to invoke a lower layer of the FUSE mechanism.
Creating a pure virtual array looks like that:

::

     auto d = LibLSS::fwrap(LibLSS::b_fused_idx<double, 2>(
        [](size_t i, size_t j)->double {
          return sqrt(i*i + j*j);
        }
     ));

This operation creates a virtual array and wraps it immediately. The
virtual array is a double bidimensional array (the two template
parameters), and infinite. Its element are computed using the provided
lambda function, which obligatorily takes 2 parameters. It is possible
to make finite virtual arrays by adding an extent parameter:

::

     auto d = LibLSS::fwrap(LibLSS::b_fused_idx<double, 2>(
        [](size_t i, size_t j)->double {
          return sqrt(i*i + j*j);
        },
        boost::extents[N][N]
     ));

Only in that case it is possible to query the dimension of the array.

Finally

**FUSED mechanism does not yet support automatic dimensional
broadcast!**

`FUSE mechanism <Category:ARES>`__
