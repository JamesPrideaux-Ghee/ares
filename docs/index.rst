Welcome to ARES's documentation!
================================
.. toctree::
   :maxdepth: 1
   :caption: Contents:
   
   building
   configuration
   ARES_Configuration_file
   ARES_Tutorials
   ARES_Code_tutorials


|a| is the main component of the Bayesian Large Scale Structure inference
pipeline.


|a| is written in C++11 and has been parallelized with OpenMP and MPI. It currently compiles with major compilers (gcc, intel, clang).

API Documentation
-----------------

The library libLSS at the base of ARES is documented using Doxygen `here <_static/doxy_html/index.html>`_.

Citing
------

If you are using |a| for your project, please cite the following articles for ARES2 and ARES3:

* Jache, Kitaura, Wandelt, 2010, MNRAS, 406, 1 (arxiv 0911.2493)
* Jasche & Lavaux, 2015, MNRAS, 447, 2 (arxiv 1402.1763)
* Lavaux & Jasche, 2016, MNRAS, 455, 3 (arxiv 1509.05040)
* Jasche & Lavaux, 2019, A&A, 625, A64 (arxiv 1806.11117)

BORG papers have a different listing.

For a full listing of publications from the Aquila consortium. Please check 
`Aquila website <https://aquila-consortium.org/publications/>`_.



Acknowledgements
----------------


This work has been funded by the following grants and institutions over the
years:

* the DFG cluster of excellenec "Origin and Structure of the Universe" 
  (http://www.universe-cluster.de).
* Institut Lagrange de Paris (grant ANR-10-LABX-63, http://ilp.upmc.fr) within 
  the context of the Idex SUPER subsidized by the French government through
  the Agence Nationale de la Recherche (ANR-11-IDEX-0004-02).
* BIG4 (ANR-16-CE23-0002) (https://big4.iap.fr)
* The "Programme National de Cosmologie et Galaxies" (PNCG, CNRS/INSU)
* Through the grant code ORIGIN, it has received support from
  the "Domaine d'Interet Majeur (DIM) Astrophysique et Conditions d'Apparitions
  de la Vie (ACAV)" from Ile-de-France region.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
