Building
========

Compiler
--------

|a| does not depend on external library as most of the important one are compiled by default. 


Optional requirements are:

* An `OpenMP <http://www.openmp.org>`_-enabled compiler (with OpenMP >= 2.0)

Compiling
---------

There is a special command line that prepares prepares build system to compile
all tools and libraries. It resides in the root directory of the ares source
tree and is called "build.sh". By default it will build everything in the 
"build" subdirectory. To get all the options please run with the option 
"--help".

After the tool has bee run, you can move to the build directory and execute
"make", which will build everything.

Please pay attention warnings and error messages. The most important are color marked.
Notably some problems may occur if two versions of the same compiler are used for C and C++.
To adjust that it is sufficient to explicitly specify the compiler path with the options '--c-compiler'
and '--cxx-compiler' of "build.sh". 

The most basic scenario for building is the following::

 $> bash build.sh
 $> cd build
 $> make all

Tips:

* Use make parallelism if possible using the '-j'option. For example ``make all -j4`` to compile using 4 parallel tasks.
* Use ``make VERBOSE=1`` to see exactly what the compilation is doing


Upon success of the compilation you will find executables in the ``src/`` subdirectory. Notably::

 $> ./src/ares3

Compilation options
^^^^^^^^^^^^^^^^^^^
