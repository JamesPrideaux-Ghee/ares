Slides of the tutorial
----------------------

.. raw:: mediawiki

   {{warning|These slides are slightly outdated. The description of the wiki is more accurate.}}

\ File:ARES_build.pdf\ 

Prerequisites
-------------

-  cmake >= 3.7
-  automake
-  libtool
-  pkg-config
-  gcc >= 7 , or intel compiler (>= 2018), or Clang (>= 7)

Python scripts has been tested with the following:

-  Python == 3.5
-  healpy == 1.10.3 (Guilhem has also a special version of healpy at )
-  HDF5Py == 2.7.0
-  Numexpr == 2.6.2
-  Numba == 0.33.0 - 0.35.0

In addition the vtktools binding in ares_tools has been used with
Paraview >= 5.2 . It should be safe to use to upload data into paraview
from numpy arrays.

Downloading and setting up for building
---------------------------------------

The first step to obtain and build ares is to clone the git repository
for bitbucket:

::

   git clone --recursive git@bitbucket.org:bayesian_lss_team/ares.git

Note that if you forget the "--recursive" option either start from
scratch or do a

.. code:: bash

    git submodule init; git submodule update

Then you may want to choose a branch that interest you. At the time of
this writing (July 25, there are 4 branches:

-  master (the pseuso stable variant of ARES)
-  guilhem/hmc_likelihood_split (the next version of master, supporting
   a wider number of algorithms for MCMC exploration and more
   abstraction)
-  guilhem/pm_cola (the next implementation of BORG-PM)
-  doogesh/altair (to support Alcock/Paczysnki test)

Normally you will want to choose master. Otherwise you may change branch
by running ``git checkout THE_BRANCH_NAME_THAT_YOU_WANT``. Once you are
on the branch that you want you may run the ``get-aquila-modules.sh``
script. The first step consists in running
``bash get-aquila-modules.sh --clone``, this will clone all the
classical Aquila private modules in the "extra/" subdirectory. The
second step is to ensure that all branches are setup correctly by
running ``bash get-aquila-modules.sh --branch-set``.

Now that the modules have been cloned and setup we may now move to
building.

As a word of caution, Do not touch the gitmodules files. Whenever you
need to do changes create a new branch in either of the main repository
or the modules and work in that branch.

4. sync submodules:
   .. code:: bash

      cd ares
      git submodule sync
      git submodule update --init --recursive

The build.sh script
-------------------

To help with the building process, there is a script called build.sh in
the top directory. It will ensure cmake is called correctly with all the
adequate parameters. At the same time it does cleaning of the build
directory if needed. The usage is the following (obtained with "bash
build.sh -h"):

.. code:: text

   Ensure the current directory is ARES
   This is the build helper. The arguments are the following:

   --cmake CMAKE_BINARY    instead of searching for cmake in the path,
     use the indicated binary

   --without-openmp        build without openmp support (default with)
   --with-mpi              build with MPI support (default without)
   --c-compiler COMPILER   specify the C compiler to use (default to cc)
   --cxx-compiler COMPILER specify the CXX compiler to use (default to c++)
   --julia JULIA_BINARY    specify the full path of julia interpreter
   --build-dir DIRECTORY   specify the build directory (default to "build/" )
   --debug                 build for full debugging
   --no-debug-log          remove all the debug output to increase speed on parallel
                           filesystem.
   --perf                  add timing instructions and report in the log files

   --extra_flags FLAGS     extra flags to pass to cmake
   --download-deps         Predownload dependencies
   --use-predownload       Use the predownloaded dependencies. They must be in
                           downloads/
   --purge                 Force purging the build directory without asking
                           questions.
   --native                Try to activate all optimizations supported by the
                           running CPU.

   Advanced usage:

   --eclipse               Generate for eclipse use
   --ninja                 Use ninja builder
   --update-tags           Update the TAGS file
   --use-system-boost      Use the boost install available from the system. This
                           reduces your footprint but also increases the
                           possibilities of miscompilation and symbol errors.
   --use-system-fftw PATH  Same but for FFTW3. We require the prefix path.
   --use-system-gsl        Same but for GSL
   --use-system-eigen PATH Same but for EIGEN. Here we require the prefix path of
                           the installation.
   --use-system-hdf5       Same but for HDF5.

   After the configuration, you can further tweak the configuration using ccmake
   (if available on your system).

Note that on some superclusters it is not possible to download files
from internet. You can only push data using SSH, but not run any wget,
curl or git pull. To account for that limitation, there are two options:
"download-deps" and "use-predownload". You should run "bash build.sh
--download-deps" on, e.g., your laptop or workstation and upload the
created "downloads" directory into the ARES source tree on the
supercomputer without touching anything inside that directory. Once you
did that you can build on the supercomputer login node, by adding
"--use-predownload" flag to build.sh in addition to others that you
need. If you want to compile with full MPI support, you have to give
'--with-mpi' as an argument to build.sh.

If you have built ARES before grabbing all the extra modules, it is fine
you can still recover your previous build. For that just go to your
build directory and run ``${CMAKE} .`` with ${CMAKE} being the cmake
executable that you have used originally. If you did not specify
anything just use 'cmake'.

A typical successful completion of the configuration ends like that:

| ``------------------------------------------------------------------``
| \ ``Configuration done.``\ 
| ``Move to /home/lavaux/PROJECTS/ares/build and type 'make' now.``
| ``Please check the configuration of your MPI C compiler. You may need``
| ``to set an environment variable to use the proper compiler.``
| ``Some example (for SH/BASH shells):``
| ``- OpenMPI:``
| ``   OMPI_CC=cc``
| ``   OMPI_CXX=c++``
| ``   export OMPI_CC OMPI_CXX``
| ``------------------------------------------------------------------``

It tells you that you should move to the build directory (by default it
is a subdirectory called "build/" in the root of the ARES source code).
There is a potential pitfall when using some MPI C compiler. They have
been installed by the system administrator to work by default with
another compiler (for example Intel C Compiler) though they work
completely fine also with another one (like GCC). In that case you have
to force the MPI C compiler to use the one that you chose with the
indicated environment variable, otherwise you will risk having
inconsistent generated code and errors at the final binary building.

``cd build ; make``

You may specify "-j" with some number as an argument to make. The number
indicates the number of CPU cores to use in parallel to compile all the
source code. We have not yet caught all the detailed dependencies and it
may happen there is a failure. Just execute 'make' again to ensure that
everything is in order (it should be).

All set! The ares and other module binaries are sitting in the
subdirectory src/.

Git submodules
--------------

Contents of file 'BASE/ares/.gitmodules'

.. code:: bash

   [submodule &quot;external/cfitsio&quot;]
       path = external/cfitsio
       url = https://github.com/healpy/cfitsio.git
   [submodule &quot;external/healpix&quot;]
       path = external/healpix
       url = https://github.com/healpy/healpixmirror.git
   [submodule &quot;external/cosmotool&quot;]
       path = external/cosmotool
       url = https://bitbucket.org/glavaux/cosmotool.git

Frequently Encountered Problems (FEP)
-------------------------------------

Non-linked files
~~~~~~~~~~~~~~~~

Problem
^^^^^^^

-  Not being able to compile after transferring to a supercluster
-  Error as following:

.. figure:: Terminal_output.png
   :alt: Terminal_output.png
   :width: 400px

   Terminal_output.png

-  Complains about not finding cfitsio in external/cfitsio while the
   cfitsio is actually in external/cfitsio.
-  Folder external/cfitsio:

.. figure:: Terminal_output-2.png
   :alt: Terminal_output-2.png
   :width: 400px

   Terminal_output-2.png

Solution
^^^^^^^^

Purging all the .o and .a in external/cfitsio, and force a rebuild of
libcfitsio by removing the file
{BUILD}/external_build/cfitsio-prefix/src/cfitsio-stamp/cfitsio-build
and type make

Building at HPC facilities
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   git clone git@bitbucket.org:bayesian_lss_team/ares.git

.. code:: bash

   cd ares
   git submodule init
   git submodule update

.. code:: bash

   bash get-aquila-modules.sh

.. code:: bash

   bash build.sh --download-deps

A typical problem is that some of the dependencies have not been
downloaded correctly. You should check if all dependencies are available
in the directory "/downloads".

Check which modules are available

.. code:: bash

   module avail

Choose the compiler or build environment. Also load the CMake module

`Building <Category:ARES.html>`__
