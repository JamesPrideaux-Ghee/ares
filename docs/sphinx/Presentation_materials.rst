This page aims at gathering useful **material for talks**. Click on the
files for details.

Light cone effects
------------------

.. figure:: Ap_doogesh.png
   :alt: Ap_doogesh.png
   :width: 400px

   Ap_doogesh.png

.. figure:: Forced_lightcone.png
   :alt: Forced_lightcone.png
   :width: 400px

   Forced_lightcone.png

.. figure:: AquilaBackground.jpg
   :alt: AquilaBackground.jpg
   :width: 400px

   AquilaBackground.jpg

SDSS3 renderings
----------------

.. figure:: Sdss3.png
   :alt: Sdss3.png
   :width: 400px

   Sdss3.png

The BORG-PM 2M++ run
--------------------

PLUS2 simulation
~~~~~~~~~~~~~~~~

.. figure:: Virgo.jpg
   :alt: Virgo.jpg
   :width: 400px

   Virgo.jpg

Reference:

-  Not published yet.

Supergalactic flows
~~~~~~~~~~~~~~~~~~~

.. figure:: Supergalactic_flows.png
   :alt: Supergalactic_flows.png
   :width: 400px

   Supergalactic_flows.png

.. figure:: Supergalactic_flows_vector.pdf
   :alt: vector variant

   vector variant

Supergalactic dark matter phase-space sheet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

File:Supergalactic_density.png%7CDark matter density (from tetrahedra
estimator) File:Supergalactic_nbstreams.png%7CNumber of dark matter
streams File:Supergalactic_vr.png%7CRadial velocity field

pdf versions: |Supergalactic_density.pdf|,
|Supergalactic_nbstreams.pdf|, |Supergalactic_vr.pdf|

Reference:

-  Paper in preparation involving F. Leclercq, R. van de Weijgaert, G.
   Lavaux, J. Jasche

The BORG SDSS run
-----------------

Evolution of cosmic structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: Borg_sdss_cosmic_evolution_small.png
   :alt: Borg_sdss_cosmic_evolution_small.png
   :width: 1000px

   Borg_sdss_cosmic_evolution_small.png

Reference:

-  

   .. raw:: mediawiki

      {{publication|J. Jasche, F. Leclercq, B. D. Wandelt|Past and present cosmic structure in the SDSS DR7 main sample|1409.6308|category=astro-ph.CO|2015JCAP...01..036J|doi=10.1088/1475-7516/2015/01/036|journal=JCAP|volume=1|pages=36|year=2015}}

Equatorial plane
~~~~~~~~~~~~~~~~

File:Sdss wedge density.png|Density field File:Sdss wedge ssd.png|Dark
matter stream density File:Sdss wedge vr.png|Radial velocity field
File:Sdss wedge density velocity.png|Density and velocity fields

Reference:

-  

   .. raw:: mediawiki

      {{publication|F. Leclercq, J. Jasche, G. Lavaux, B. Wandelt, W. Percival|The phase-space structure of nearby dark matter as constrained by the SDSS|1601.00093|category=astro-ph.CO|2017JCAP...06..049L|doi=10.1088/1475-7516/2017/06/049|journal=JCAP|volume=6|pages=49|year=2017}}

Supergalactic plane
~~~~~~~~~~~~~~~~~~~

.. figure:: Supergalactic.png
   :alt: Supergalactic.png
   :width: 1000px

   Supergalactic.png

Reference:

-  

   .. raw:: mediawiki

      {{publication|F. Leclercq, J. Jasche, G. Lavaux, B. Wandelt, W. Percival|The phase-space structure of nearby dark matter as constrained by the SDSS|1601.00093|category=astro-ph.CO|2017JCAP...06..049L|doi=10.1088/1475-7516/2017/06/049|journal=JCAP|volume=6|pages=49|year=2017}}

DIVA structures
~~~~~~~~~~~~~~~

File:Diva pdf final vs.png File:Diva pdf final fc.png

References:

-  

   .. raw:: mediawiki

      {{publication|F. Leclercq, J. Jasche, G. Lavaux, B. Wandelt, W. Percival|The phase-space structure of nearby dark matter as constrained by the SDSS|1601.00093|category=astro-ph.CO|2017JCAP...06..049L|doi=10.1088/1475-7516/2017/06/049|journal=JCAP|volume=6|pages=49|year=2017}}

-  

   .. raw:: mediawiki

      {{publication|F. Leclercq, G. Lavaux, J. Jasche, B. Wandelt|Comparing cosmic web classifiers using information theory|1606.06758|category=astro-ph.CO|2016JCAP...08..027L|doi=10.1088/1475-7516/2016/08/027|journal=JCAP|volume=8|pages=27|year=2016}}

Simbelmynë
----------

An illustration of the simulator `Simbelmynë <Simbelmynë>`__:

.. figure:: Simbelmyne_example.png
   :alt: Simbelmyne_example.png
   :width: 1000px

   Simbelmyne_example.png

`Category:Aquila Consortium <Category:Aquila_Consortium>`__

.. |Supergalactic_density.pdf| image:: Supergalactic_density.pdf
.. |Supergalactic_nbstreams.pdf| image:: Supergalactic_nbstreams.pdf
.. |Supergalactic_vr.pdf| image:: Supergalactic_vr.pdf

