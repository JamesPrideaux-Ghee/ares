/*+
    ARES/HADES/BORG Package -- -- ./libLSS/physics/generic_cic.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016-2018)
       Jens Jasche <j.jasche@tum.de> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_GENERIC_CIC_HPP
#define __LIBLSS_GENERIC_CIC_HPP

#include <boost/config.hpp>

namespace LibLSS {

    namespace CIC_Tools {

        struct Periodic {
            int N0, N1, N2;

            Periodic(int fN0, int fN1, int fN2) :
                N0(fN0), N1(fN1), N2(fN2) {}

            void operator()(size_t& i, size_t& j, size_t& k) const {
                if (i>=N0) i %= N0;
                if (j>=N1) j %= N1;
                if (k>=N2) k %= N2;
            }
        };

        struct Periodic_MPI {
            size_t N0, N1, N2;

            Periodic_MPI(size_t fN0, size_t fN1, size_t fN2, MPI_Communication *comm) :
                N0(fN0), N1(fN1), N2(fN2) {}

            void operator()(size_t& i, size_t& j, size_t& k) const {
                if (j>=N1) j %= N1;
                if (k>=N2) k %= N2;
            }
        };

        struct DefaultWeight  {
            BOOST_STATIC_CONSTANT(size_t, dimensionality = 1);
            double operator[](long) const { return 1; }
        };
    }


    template<typename T,typename ImplType>
    class GenericCIC {
    public:
        typedef ImplType impl;

        template<typename ParticleArray, typename ProjectionDensityArray, typename WeightArray,
                 typename PeriodicFunction >
        static void projection(const ParticleArray& particles, ProjectionDensityArray& density,
                               T Lx, T Ly, T Lz,
                               int N0, int N1, int N2,
                               const PeriodicFunction& p, const WeightArray& weight, size_t Np) {
            impl::projection(particles, density, Lx, Ly, Lz, N0, N1, N2, p, weight, Np);
        }


        template<typename ParticleArray, typename ProjectionDensityArray, typename WeightArray,
                 typename PeriodicFunction >
        static void projection(const ParticleArray& particles, ProjectionDensityArray& density,
                               T Lx, T Ly, T Lz,
                               int N0, int N1, int N2,
                               const PeriodicFunction& p, const WeightArray& weight) {
          impl::projection(particles, density, Lx, Ly, Lz, N0, N1, N2, p, weight, particles.shape()[0]);
        }

        template<typename ParticleArray, typename ProjectionDensityArray, typename PeriodicFunction>
        static void projection(const ParticleArray& particles, ProjectionDensityArray& density,
                               T Lx, T Ly, T Lz,
                               int N0, int N1, int N2,
                               const PeriodicFunction& periodic) {
          impl::projection(particles, density, Lx, Ly, Lz, N0, N1, N2, periodic,
                       CIC_Tools::DefaultWeight(), particles.shape()[0]);
        }

        template<typename ParticleArray, typename ProjectionDensityArray>
        static void projection(const ParticleArray& particles, ProjectionDensityArray& density,
                               T Lx, T Ly, T Lz,
                               int N0, int N1, int N2) {
          impl::projection(particles, density, Lx, Ly, Lz, N0, N1, N2, CIC_Tools::Periodic(N0, N1, N2),
                           CIC_Tools::DefaultWeight(), particles.shape()[0]);
        }


        template<typename ParticleArray, typename GradientArray, typename ProjectionDensityArray, typename PeriodicFunction>
        static void adjoint(const ParticleArray& particles, ProjectionDensityArray& density,
                            GradientArray& adjoint_gradient,
                            T Lx, T Ly, T Lz,
                            int N0, int N1, int N2,
                            const PeriodicFunction& p,
                            T nmean, size_t Np) {
            impl::adjoint(particles, density, adjoint_gradient,CIC_Tools::DefaultWeight(), Lx, Ly, Lz, N0, N1, N2, p, nmean, Np);
        }

        template<typename ParticleArray, typename GradientArray, typename ProjectionDensityArray, typename PeriodicFunction>
        static void adjoint(const ParticleArray& particles, ProjectionDensityArray& density,
                            GradientArray& adjoint_gradient,
                            T Lx, T Ly, T Lz,
                            int N0, int N1, int N2,
                            const PeriodicFunction& p,
                            T nmean) {
            impl::adjoint(particles, density, adjoint_gradient,CIC_Tools::DefaultWeight(), Lx, Ly, Lz, N0, N1, N2, p, nmean, particles.shape()[0]);
        }

        template<typename ParticleArray, typename GradientArray, typename ProjectionDensityArray>
        static void adjoint(const ParticleArray& particles, ProjectionDensityArray& density,
                            GradientArray& adjoint_gradient,
                            T Lx, T Ly, T Lz,
                            int N0, int N1, int N2,
                            T nmean) {
          impl::adjoint(particles, density, adjoint_gradient,CIC_Tools::DefaultWeight(), Lx, Ly, Lz, N0, N1, N2, CIC_Tools::Periodic(N0, N1, N2), nmean, particles.shape()[0]);
        }

    };

}

#endif
