/*+
+*/
#ifndef __LIBLSS_HDF5_TYPE_HPP
#define __LIBLSS_HDF5_TYPE_HPP

#include <CosmoTool/hdf5_array.hpp>

namespace LibLSS
{
   using CosmoTool::H5_CommonFileGroup;
}

#endif

// ARES TAG: authors_num = 2
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
// ARES TAG: year(0) = 2019
