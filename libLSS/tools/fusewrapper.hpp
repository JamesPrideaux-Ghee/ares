/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/fusewrapper.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2017-2019)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_FUSEWRAPPER_HPP
#define __LIBLSS_FUSEWRAPPER_HPP

#include <type_traits>
#include <utility>
#include <boost/core/enable_if.hpp>
#include <boost/type_traits/is_scalar.hpp>
#include "libLSS/tools/fused_array.hpp"
#include "libLSS/tools/fused_assign.hpp"
#include "libLSS/tools/phoenix_vars.hpp"
#include <boost/phoenix/operator.hpp>
#include "libLSS/tools/fused_reduce.hpp"
#include "libLSS/tools/fused_cond.hpp"
#include <boost/tti/has_type.hpp>
#include <boost/tti/has_member_function.hpp>
#include <boost/mpl/and.hpp>
#include <boost/mpl/logical.hpp>
#include "libLSS/tools/array_concepts.hpp"
#include <CosmoTool/algo.hpp>

namespace LibLSS {

  namespace FuseWrapper_detail {

    using LibLSS::array_concepts::is_array_like;
    using LibLSS::array_concepts::is_array_storage;
    using LibLSS::array_concepts::is_array_sub;
    using LibLSS::array_concepts::is_array_view;
    using LibLSS::array_concepts::is_callable;

    template <typename Array, bool copy>
    struct Wrapper;

    template <
        typename Array,
        typename U = typename std::remove_reference<Array>::type>
    Wrapper<U, true> fwrap_(Array &&a, std::true_type);
    template <
        typename Array,
        typename U = typename std::remove_reference<Array>::type>
    Wrapper<U, false> fwrap_(Array &&a, std::false_type);

    template <typename Array>
    auto fwrap(Array &&a) -> decltype(
        fwrap_(std::forward<Array>(a), std::is_rvalue_reference<Array &&>())) {
      return fwrap_(
          std::forward<Array>(a), std::is_rvalue_reference<Array &&>());
    }

    template <typename A, bool copy>
    struct CopyType;

    template <typename A>
    struct CopyType<A, true> {
      typedef A type;
      typedef A const_type;
    };

    template <typename A>
    struct CopyType<A, false> {
      typedef A &type;
      typedef A const &const_type;
    };

    template <typename T>
    struct constantFunctor {
      T value;

      constantFunctor(T v) : value(v) {}

      template <typename... Args>
      T const &operator()(Args &&... a) const {
        return value;
      }
    };

    template <typename F>
    struct singleFunctor {
      typedef std::result_of<F()> Result;
      F f;

      singleFunctor() {}
      singleFunctor(F f_) : f(f_) {}

      template <typename... Args>
      Result operator()(Args &&... a) const {
        return f();
      }
    };

    template <typename Array, bool copy>
    struct Wrapper {
      typedef Array array_t;
      typedef typename CopyType<Array, copy>::type WType;
      typedef typename CopyType<Array, copy>::const_type WTypeConst;
      WType a;

      explicit Wrapper(Array &a_) : a(a_) {}

      explicit Wrapper(Array &&a_) : a(a_) {}

      Array &operator*() { return a; }
      Array const &operator*() const { return a; }
      Array *operator->() { return &a; }
      Array const *operator->() const { return &a; }

      // This auxiliary function creates a perfect
      // forwarding of the required encapsulation of the array.
      // If a copy is needed to ensure the object is long lived
      // enough, then WType is the full Array object.
      // Otherwise, it will be a reference on the original object.
      // Thus a receiving function will get either a lvalue-ref or an rvalue-ref
      // depending on the need.
      WType forward_wrap() { return a; }
      WTypeConst forward_wrap() const { return a; }

      template <typename Array2, bool B2>
      static inline typename Wrapper<Array2, B2>::WType const
      fautowrap(const Wrapper<Array2, B2> &other) {
        return other.a;
      }

      template <typename Array2, typename U = Array2>
      static inline
          typename std::enable_if<is_array_like<Array2>::value, U>::type const &
          fautowrap(Array2 const &other) {
        return other;
      }

      // Intel 17.2 C++ compiler crashes without those
      template <typename ValueType, size_t N>
      struct wrapconst {
        typedef decltype(b_va_fused<ValueType, N>(
            constantFunctor<ValueType>(ValueType(0)))) Result;

        static inline Result wrap(ValueType value) {
          return (b_va_fused<ValueType, N>(constantFunctor<ValueType>(value)));
        }
      };

      template <typename ValueType, size_t N, typename Operator>
      struct wrapfunc {
        typedef decltype(
            b_va_fused<ValueType, N>(singleFunctor<Operator>())) Result;

        static inline Result wrap(Operator op) {
          return (b_va_fused<ValueType, N>(singleFunctor<Operator>(op)));
        }
      };

      template <typename ValueType, typename U = ValueType>
      static inline typename std::enable_if<
          (boost::is_arithmetic<ValueType>::value ||
           array_concepts::is_complex_type<ValueType>::value) &&
              is_array_like<Array>::value,
          wrapconst<U, Array::dimensionality>>::type::Result
      fautowrap(ValueType other) {
        return wrapconst<ValueType, Array::dimensionality>::wrap(other);
      }

      template <typename F, typename U = F>
      static inline typename boost::enable_if<
          is_callable<F>,
          wrapfunc<typename Array::element, Array::dimensionality, U>>::type::
          Result
          fautowrap(F other) {
        return wrapfunc<
            typename Array::element, Array::dimensionality, F>::wrap(other);
      }

      typename Array::element sum() const {
        return LibLSS::reduce_sum<typename Array::element>(a);
      }

      typename Array::element min() const {
        return LibLSS::reduce_min<typename Array::element>(a);
      }

      typename Array::element max() const {
        return LibLSS::reduce_max<typename Array::element>(a);
      }

      template <typename ArrayTo>
      Wrapper<Array, copy> const &copy_to(ArrayTo &to) const {
        LibLSS::copy_array(to, a);
        return *this;
      }

      Wrapper<Array, copy> &operator=(Wrapper<Array, copy> &other) {
        return operator=((Wrapper<Array, copy> const &)other);
      }

      template <typename Wrap2, typename A = Array>
      inline Wrapper<Array, copy> &operator=(Wrap2 const &other) {
        static_assert(
            is_array_storage<A>::value || is_array_view<A>::value ||
                is_array_sub<A>::value,
            "The wrapped array is likely a pure expression. Impossible to "
            "assign.");
        LibLSS::copy_array(a, fautowrap(other));
        return *this;
      }
    };

    template <typename T>
    struct is_wrapper : std::false_type {};

    template <typename Array, bool copy>
    struct is_wrapper<Wrapper<Array, copy>> : std::true_type {};

    // Here we builds recursively lazy binary expressions using boost::phoenix capabilities.
    //
    template <typename T, typename U>
    using DisableIf = typename std::enable_if<!T::value, U>::type;

    template <typename T, typename U>
    using EnableIf = typename std::enable_if<T::value, U>::type;

    template <typename A, typename = int>
    struct _DetectElement;

    template <typename A>
    struct _DetectElement<
        A, typename std::enable_if<
               !is_array_like<A>::value && !is_wrapper<A>::value, int>::type> {
      typedef typename std::remove_reference<A>::type element;
    };

    template <typename A>
    struct _DetectElement<A, EnableIf<is_array_like<A>, int>> {
      typedef typename std::remove_reference<A>::type::element element;
    };

    template <typename Array, bool copy>
    struct _DetectElement<Wrapper<Array, copy>> {
      typedef typename Array::element element;
    };

    template <typename A>
    using DetectElement = typename _DetectElement<
        typename std::remove_reference<A>::type>::element;

#define FWRAPPER_BUILD_UNARY_OPERATOR(op)                                      \
  template <typename Array, bool copy>                                         \
  inline auto operator op(Wrapper<Array, copy> const &self) {                  \
    return fwrap(                                                              \
        b_va_fused<typename Array::element>(op _p1, self.forward_wrap()));     \
  }

    FWRAPPER_BUILD_UNARY_OPERATOR(-)
    FWRAPPER_BUILD_UNARY_OPERATOR(!)

#undef FWRAPPER_BUILD_UNARY_OPERATOR

#define FWRAPPER_BUILD_BINARY_OPERATOR(op)                                     \
  template <typename Array, bool copy, typename ToWrap2>                       \
  inline auto operator op(                                                     \
      Wrapper<Array, copy> const &self, ToWrap2 const &other) {                \
    typedef typename Array::element A_t;                                       \
    typedef DetectElement<ToWrap2> O_t;                                        \
    typedef decltype(::std::declval<A_t>() op ::std::declval<O_t>()) AO_t;     \
    return fwrap(b_va_fused<AO_t>(                                             \
        _p1 op _p2, self.forward_wrap(),                                       \
        Wrapper<Array, copy>::fautowrap(other)));                              \
  }                                                                            \
                                                                               \
  template <                                                                   \
      typename Array, bool copy, typename NotWrap2,                            \
      typename T = DisableIf<is_wrapper<NotWrap2>, void>>                      \
  inline auto operator op(                                                     \
      NotWrap2 const &other, Wrapper<Array, copy> const &self) {               \
    typedef typename Array::element A_t;                                       \
    typedef DetectElement<NotWrap2> O_t;                                       \
    typedef decltype(::std::declval<A_t>() op ::std::declval<O_t>()) AO_t;     \
    return fwrap(b_va_fused<AO_t>(                                             \
        _p2 op _p1, self.forward_wrap(),                                       \
        Wrapper<Array, copy>::fautowrap(other)));                              \
  }

    FWRAPPER_BUILD_BINARY_OPERATOR(+);
    FWRAPPER_BUILD_BINARY_OPERATOR(-);
    FWRAPPER_BUILD_BINARY_OPERATOR(/);
    FWRAPPER_BUILD_BINARY_OPERATOR(*);
    FWRAPPER_BUILD_BINARY_OPERATOR (^);

#undef FWRAPPER_BUILD_BINARY_OPERATOR

#define FWRAPPER_BUILD_COMPARATOR(op)                                          \
  template <typename Array, bool copy, typename Wrap2>                         \
  inline auto operator op(                                                     \
      Wrapper<Array, copy> const &self, Wrap2 const &other)                    \
      ->decltype(fwrap(b_va_fused<bool>(                                       \
          _p1 op _p2, self.forward_wrap(),                                     \
          Wrapper<Array, copy>::fautowrap(other)))) {                          \
    return fwrap(b_va_fused<bool>(                                             \
        _p1 op _p2, self.forward_wrap(),                                       \
        Wrapper<Array, copy>::fautowrap(other)));                              \
  }                                                                            \
  template <typename Array, bool copy, typename NotWrap2>                      \
  inline auto operator op(                                                     \
      NotWrap2 const &other, Wrapper<Array, copy> const &self)                 \
      ->DisableIf<                                                             \
          is_wrapper<NotWrap2>,                                                \
          decltype(fwrap(b_va_fused<bool>(                                     \
              _p2 op _p1, self.forward_wrap(),                                 \
              Wrapper<Array, copy>::fautowrap(other))))> {                     \
    return fwrap(b_va_fused<bool>(                                             \
        _p2 op _p1, self.forward_wrap(),                                       \
        Wrapper<Array, copy>::fautowrap(other)));                              \
  }

    FWRAPPER_BUILD_COMPARATOR(==)
    FWRAPPER_BUILD_COMPARATOR(!=)
    FWRAPPER_BUILD_COMPARATOR(>)
    FWRAPPER_BUILD_COMPARATOR(<)
    FWRAPPER_BUILD_COMPARATOR(<=)
    FWRAPPER_BUILD_COMPARATOR(>=)
    FWRAPPER_BUILD_COMPARATOR(&&)
    FWRAPPER_BUILD_COMPARATOR(||)

#undef FWRAPPER_BUILD_COMPARATOR

    //    template<typename Array >
    //    Wrapper<Array const,false> fwrap(Array const& a) {
    //        return Wrapper<Array const,false>(a);
    //    }
    //

    template <typename Array, typename U>
    Wrapper<U, true> fwrap_(Array &&a, std::true_type) {
      // lvalue refs, copy mandatorily
      return Wrapper<U, true>(a);
    }

    template <typename Array, typename U>
    Wrapper<U, false> fwrap_(Array &&a, std::false_type) {
      // rvalue refs, do not copy
      return Wrapper<U, false>(a);
    }

  } // namespace FuseWrapper_detail

  using FuseWrapper_detail::fwrap;
  using FuseWrapper_detail::is_wrapper;

} // namespace LibLSS

#define FUSE_MATH_UNARY_OPERATOR(mathfunc)                                     \
  namespace LibLSS {                                                           \
    namespace FuseWrapper_detail {                                             \
      template <typename T>                                                    \
      struct mathfunc##_functor {                                              \
        auto operator()(T const &val) const -> decltype(std::mathfunc(val)) {  \
          return std::mathfunc(val);                                           \
        }                                                                      \
      };                                                                       \
      template <typename T>                                                    \
      using result_##mathfunc =                                                \
          typename std::result_of<mathfunc##_functor<T>(T)>::type;             \
    }                                                                          \
  }                                                                            \
                                                                               \
  namespace std {                                                              \
    template <typename Array, bool copy>                                       \
    auto mathfunc(LibLSS::FuseWrapper_detail::Wrapper<Array, copy> wrap) {     \
      typedef LibLSS::FuseWrapper_detail::result_##mathfunc<                   \
          typename Array::element>                                             \
          Return;                                                              \
      typedef LibLSS::FuseWrapper_detail::mathfunc##_functor<                  \
          typename Array::element>                                             \
          Functor;                                                             \
      return LibLSS::fwrap(                                                    \
          LibLSS::b_va_fused<Return>(Functor(), wrap.forward_wrap()));         \
    }                                                                          \
  }

//// std::cout << "Functor=" #mathfunc << " type = " << typeid(Return).name() << std::endl;\
//
//

#define FUSE_MATH_BINARY_OPERATOR(mathfunc)                                    \
  namespace LibLSS {                                                           \
    namespace FuseWrapper_detail {                                             \
      template <typename T, typename T2>                                       \
      struct mathfunc##_functor {                                              \
        T2 val2;                                                               \
                                                                               \
        mathfunc##_functor(T2 val2_) : val2(val2_) {}                          \
        auto operator()(T const &val) const                                    \
            -> decltype(std::mathfunc(val, val2)) {                            \
          return std::mathfunc(val, val2);                                     \
        }                                                                      \
      };                                                                       \
      template <typename T, typename T2>                                       \
      using result_##mathfunc =                                                \
          typename std::result_of<mathfunc##_functor<T, T2>(T)>::type;         \
    }                                                                          \
  }                                                                            \
                                                                               \
  namespace std {                                                              \
    template <                                                                 \
        typename Array, bool copy, typename Other,                             \
        typename = typename boost::enable_if<boost::is_scalar<Other>>::type>   \
    auto mathfunc(                                                             \
        LibLSS::FuseWrapper_detail::Wrapper<Array, copy> wrap, Other other) {  \
      return LibLSS::fwrap(                                                    \
          LibLSS::b_va_fused<LibLSS::FuseWrapper_detail::result_##mathfunc<    \
              typename Array::element, Other>>(                                \
              LibLSS::FuseWrapper_detail::mathfunc##_functor<                  \
                  typename Array::element, Other>(other),                      \
              wrap.forward_wrap()));                                           \
    }                                                                          \
  }

FUSE_MATH_UNARY_OPERATOR(real);
FUSE_MATH_UNARY_OPERATOR(imag);
FUSE_MATH_UNARY_OPERATOR(norm);
FUSE_MATH_UNARY_OPERATOR(conj);
FUSE_MATH_UNARY_OPERATOR(exp);
FUSE_MATH_UNARY_OPERATOR(sinh);
FUSE_MATH_UNARY_OPERATOR(cosh);
FUSE_MATH_UNARY_OPERATOR(tanh);
FUSE_MATH_UNARY_OPERATOR(sin);
FUSE_MATH_UNARY_OPERATOR(cos);
FUSE_MATH_UNARY_OPERATOR(tan);
FUSE_MATH_UNARY_OPERATOR(atan);
FUSE_MATH_UNARY_OPERATOR(acos);
FUSE_MATH_UNARY_OPERATOR(asin);
FUSE_MATH_UNARY_OPERATOR(sqrt);
FUSE_MATH_UNARY_OPERATOR(log);
FUSE_MATH_UNARY_OPERATOR(log10);
FUSE_MATH_UNARY_OPERATOR(floor);
FUSE_MATH_UNARY_OPERATOR(ceil);
FUSE_MATH_UNARY_OPERATOR(abs);
FUSE_MATH_UNARY_OPERATOR(erf);
FUSE_MATH_UNARY_OPERATOR(erfc);
FUSE_MATH_BINARY_OPERATOR(pow);
FUSE_MATH_BINARY_OPERATOR(modf);
FUSE_MATH_BINARY_OPERATOR(fmod);

namespace LibLSS {
  template <
      size_t exponent, typename T,
      typename = typename boost::enable_if<
          boost::is_scalar<typename boost::remove_reference<T>::type>>::type>
  auto ipow(T &&t) {
    return CosmoTool::spower<
        exponent, typename boost::remove_reference<T>::type>(t);
  }

  template <size_t N, typename T>
  struct _spower_helper {
    typedef decltype(CosmoTool::spower<N, T>(T(0))) Return;

    inline Return operator()(T a) const { return CosmoTool::spower<N, T>(a); }
  };

  template <size_t exponent, typename Array, bool copy>
  auto ipow(LibLSS::FuseWrapper_detail::Wrapper<Array, copy> wrap) {
    return fwrap(
        b_va_fused<
            typename _spower_helper<exponent, typename Array::element>::Return>(
            _spower_helper<exponent, typename Array::element>(),
            std::forward<typename LibLSS::FuseWrapper_detail::Wrapper<
                Array, copy>::WType>(wrap.a)));
  }

  template <typename T, size_t Nd, typename ExtentType>
  auto ones(ExtentType e) {
    return fwrap(b_fused_idx<T, Nd>([](auto... x) { return T(1); }, e));
  }

  template <typename T, size_t Nd, typename ExtentType>
  auto zero(ExtentType e) {
    return fwrap(b_fused_idx<T, Nd>([](auto... x) { return T(0); }, e));
  }

  template <
      typename ArrayM, bool copyM, typename Array1, bool copy1, typename Array2,
      bool copy2>
  auto mask(
      LibLSS::FuseWrapper_detail::Wrapper<ArrayM, copyM> wrap_mask,
      LibLSS::FuseWrapper_detail::Wrapper<Array1, copy1> array1,
      LibLSS::FuseWrapper_detail::Wrapper<Array2, copy2> array2) {
    return fwrap(b_cond_fused<typename Array1::element>(
        wrap_mask.forward_wrap(), array1.forward_wrap(),
        array2.forward_wrap()));
  }
} // namespace LibLSS

#endif
