/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/push_operators.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_PUSH_OPERATORS
#define __LIBLSS_PUSH_OPERATORS

namespace LibLSS {

    template<bool accum> 
    struct push_to {
        template<typename T>
        static void apply(T& ref, const T& value);
        
        template<typename T>
        void operator()(T& ref, const T& value) {
            apply(ref, value);
        }        
    };

    template<>
    struct push_to<true> {
        template<typename T>
        static void apply(T& ref, const T& value) {
            ref += value;
        }
    };

    template<>
    struct push_to<false> {
        template<typename T>
        static void apply(T& ref, const T& value) {
            ref = value;
        }
    };

}

#endif
