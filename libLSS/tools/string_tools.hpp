/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/string_tools.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_STRING_TOOLS_HPP
#define __LIBLSS_STRING_TOOLS_HPP

#include <string>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <vector>
#include <boost/multi_array.hpp>
#include "libLSS/tools/is_stl_container.hpp"

namespace LibLSS {

  using std::to_string;

  template <typename T>
  typename std::enable_if<is_stl_container_like<T>::value, std::string>::type
  to_string(T const &V) {
    std::ostringstream s;

    std::copy(
        V.begin(), V.end(),
        std::ostream_iterator<typename T::value_type>(s, ","));
    return s.str();
  }

  std::vector<std::string>
  tokenize(std::string const &in, std::string const &separator);
  /*  template<typename T>
  std::string to_string(boost::multi_array_ref<T,1> const& V) {
    std::ostringstream s;

    std::copy(V.begin(), V.end(), std::ostream_iterator<T>(s, ","));
    return s.str();
  }*/
} // namespace LibLSS

#endif
