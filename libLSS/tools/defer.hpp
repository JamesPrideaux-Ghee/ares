/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/defer.hpp
    Copyright (C) 2014-2018 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2018 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_TOOLS_DEFER_HPP
#  define __LIBLSS_TOOLS_DEFER_HPP

#  include <functional>
#  include <vector>
#  include "libLSS/tools/console.hpp"

namespace LibLSS {

  /**
   * @brief Add Future like concept for ARES.
   * If some state is not guaranteed to exist at a given time, a Defer can be
   * added to a class. Other class may subscribe to this Defer to receive asynchronously
   * an information on the change of status of the Defer.
   */
  class Defer {
  protected:
    typedef std::function<void()> Function;
    std::vector<Function> ready_f, error_f;
    enum State { WAIT, READY, ERROR };
    State state;

  public:
    /**
     * @brief Construct a new Defer object
     * 
     */
    Defer() : state(WAIT) {}

    /**
     * @brief Destroy the Defer object
     * 
     */
    ~Defer() {}

    /**
     * @brief Check whether the defer is still is waiting status.
     * 
     * @return true if it is still waiting.
     * @return false if it is READY or FAIL.
     */
    bool isWaiting() const { return state == WAIT; }

    /**
     * @brief Subscribe a new functor to be called when state is becoming ready.
     * 
     * @param f Functor to be called
     */
    void ready(Function f) {
      if (state == READY) {
        f();
        return;
      } else if (state == WAIT) {
        ready_f.push_back(f);
      }
    }

    /**
     * @brief Subscribe a new functor to be called when state is becoming fail.
     * 
     * @param f Functor to be called.
     */
    void fail(Function f) {
      if (state == ERROR) {
        f();
        return;
      } else if (state == WAIT) {
        error_f.push_back(f);
      }
    }

    /**
     * @brief Submit a state change to READY.
     * 
     */
    void submit_ready() {
      if (state == READY)
        return;

      Console::instance().c_assert(
          state == WAIT, "State has already changed (in submit_ready).");
      state = READY;
      for (auto &f : ready_f) {
        f();
      }
      ready_f.clear();
    }

    /**
     * @brief Submit a state change to FAIL.
     * 
     */
    void submit_error() {
      if (state == ERROR)
        return;

      Console::instance().c_assert(
          state == WAIT, "State has already changed (in submit_error).");
      state = ERROR;
      for (auto &f : error_f) {
        f();
      }
      error_f.clear();
    }
  };

} // namespace LibLSS

#endif
// ARES TAG: num_authors = 1
// ARES TAG: name(0) = Guilhem Lavaux
// ARES TAG: email(0) = guilhem.lavaux@iap.fr
// ARES TAG: year(0) = 2018-2019
