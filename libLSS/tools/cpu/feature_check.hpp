/*+
+*/
#ifndef __LIBLSS_TOOLS_CPUCHECK_HPP
#define __LIBLSS_TOOLS_CPUCHECK_HPP

#if defined(__GNUC__) && !defined(__INTEL_COMPILER)
#  include "feature_check_gnuc.hpp"
#else
#  include "feature_check_other.hpp"
#endif

#endif
