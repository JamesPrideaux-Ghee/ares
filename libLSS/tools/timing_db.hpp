#ifndef __LIBLSS_TOOLS_TIMING_DB_HPP
#define __LIBLSS_TOOLS_TIMING_DB_HPP

#include <CosmoTool/hdf5_array.hpp>
#include "libLSS/tools/hdf5_type.hpp"

namespace LibLSS {

  namespace timings {
    void load(H5_CommonFileGroup& g);
    void save(H5_CommonFileGroup& g);
  }

}

#endif
