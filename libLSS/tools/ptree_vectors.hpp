/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/ptree_vectors.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_PTREE_VECTORS_HPP
#define __LIBLSS_PTREE_VECTORS_HPP

#include <string>
#include <boost/lexical_cast.hpp>
#include <vector>
#include "libLSS/tools/string_tools.hpp"

namespace LibLSS {

  template<typename T>
  std::vector<T> string_as_vector(const std::string& s, std::string const& separator)
  {
    auto tokens = tokenize(s, separator);
    std::vector<T> result;
    std::transform(tokens.begin(), tokens.end(), std::back_inserter(result),
		   [](std::string const& s)->T { return boost::lexical_cast<T>(s); });
    return result;
  }
}

#endif
