/*+
    ARES/HADES/BORG Package -- -- ./libLSS/samplers/core/main_loop.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Franz Elsner <felsner@nca-08.MPA-Garching.MPG.DE> (2017)
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)
       elsner <f.elsner@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#include "libLSS/tools/console.hpp"
#include "libLSS/samplers/core/main_loop.hpp"
#include "libLSS/tools/timing_db.hpp"

using namespace LibLSS;
using std::string;

MainLoop::MainLoop()
{
    show_splash();
    mcmc_id = 0;
}

MainLoop::~MainLoop()
{
}

void MainLoop::show_splash()
{
}

void MainLoop::initialize()
{
    Console& cons = Console::instance();

    cons.print<LOG_STD>("Initializing samplers");
    cons.indent();
    
    for (MCList::iterator i = mclist.begin(); i != mclist.end(); ++i) {    
        i->first->init_markov(state);
    }
    cons.unindent();
    cons.print<LOG_STD>("Done");
}

void MainLoop::snap()
{
    using boost::str;
    using boost::format;
    MPI_Communication *comm = MPI_Communication::instance();

    if (comm->rank() != 0) {
        string tmpname(str(format("tmp_mcmc_%d.h5_rank_%d") % mcmc_id % comm->rank()));
        H5::H5File f(tmpname, H5F_ACC_TRUNC);
        state.mpiSaveState(f, comm, false, true);
        unlink(tmpname.c_str());
    } else {
        H5::H5File f(str(format("mcmc_%d.h5") % mcmc_id), H5F_ACC_TRUNC);
        state.mpiSaveState(f, comm, false, true);
    }
    mcmc_id++;
}

void MainLoop::save()
{
  using boost::str;
  using boost::format;
  MPI_Communication *comm = MPI_Communication::instance();
  string fname_final = str(format("restart.h5_%d") % comm->rank());
  string fname_build = fname_final + "_build";
  
    {
        H5::H5File f(fname_build, H5F_ACC_TRUNC);
        state.saveState(f);
        timings::save(f);
    }
    comm->barrier();
    
    rename(fname_build.c_str(), fname_final.c_str());  
}

void MainLoop::save_crash()
{
  using boost::str;
  using boost::format;
  MPI_Communication *comm = MPI_Communication::instance();
  string fname_final = str(format("crash_file.h5_%d") % comm->rank());
  string fname_build = fname_final + "_build";
  
    {
        H5::H5File f(fname_build, H5F_ACC_TRUNC);
        state.saveState(f);
    }
    
    rename(fname_build.c_str(), fname_final.c_str());  
}


void MainLoop::run()
{
    ConsoleContext<LOG_STD> ctx("MainLoop::run");
    int count = 0;
    Progress<LOG_STD> progress = 
      Console::instance().start_progress<LOG_STD>("Main loop iteration", mclist.size(), 30);
    for (MCList::iterator i = mclist.begin(); i != mclist.end(); ++i) {    
        int looping = i->second;
        for (int j = 0; j < looping; j++)
            i->first->sample(state);
        count++;
        progress.update(count);
    }
    progress.destroy();
}

void MainLoop::restore(const std::string& fname, bool flexible)
{
    Console& cons = Console::instance();
    MPI_Communication *comm = MPI_Communication::instance();
    string fname_full = 
      flexible ? fname : (boost::str(boost::format("%s_%d") % fname % comm->rank()));
    H5::H5File f(fname_full, 0);
    ConsoleContext<LOG_INFO> ctx("restoration of MCMC state");
    
    if (flexible)
      Console::instance().print<LOG_WARNING>("Using flexible mechanism");
    
    ctx.print("Initialize variables");
    for (MCList::iterator i = mclist.begin(); i != mclist.end(); ++i) {    
        i->first->restore_markov(state);
    }
    
    ctx.print("Load markov state from file");
    {
        state.restoreState(f, flexible);
    }

    timings::load(f);
}
