/*+
    ARES/HADES/BORG Package -- -- ./libLSS/mpi/generic_mpi.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Florian Führer <fuhrer@iap.fr> (2018)
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2014-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifdef ARES_MPI_FFTW 
#define OMPI_SKIP_MPICXX
#define _MPICC_H
#include <mpi.h>
#include "real_mpi/mpi_type_translator.hpp"
#include "real_mpi/mpi_communication.hpp"

#ifndef __LIBLSS_MPI_REAL_DEFINED
#define __LIBLSS_MPI_REAL_DEFINED
namespace LibLSS {
  static constexpr bool MPI_IS_REAL = true;
}
#endif

#else
#include "fake_mpi/mpi_type_translator.hpp"
#include "fake_mpi/mpi_communication.hpp"

#ifndef __LIBLSS_MPI_REAL_DEFINED
#define __LIBLSS_MPI_REAL_DEFINED
namespace LibLSS {
  static constexpr bool MPI_IS_REAL = false;
}
#endif
#endif

