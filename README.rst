===============================================
ARES: Algorithm for REconstruction and Sampling
===============================================

Copyright(c) 2009-2019 Jens Jasche, 2014-2019 Guilhem Lavaux

Description
-----------

This is the main component of the Bayesian Large Scale Structure inference
pipeline.

A lot of complementary informations are available on the wiki https://www.aquila-consortium.org/wiki/index.php

Building
--------

There is a special command line that prepares prepares build system to compile
all tools and libraries. It resides in the root directory of the ares source
tree and is called "build.sh". By default it will build everything in the 
"build" subdirectory. To get all the options please run with the option 
"--help".

After the tool has bee run, you can move to the build directory and execute
"make", which will build everything.

Please pay attention warnings and error messages. The most important are color marked.
Notably some problems may occur if two versions of the same compiler are used for C and C++.
To adjust that it is sufficient to explicitly specify the compiler path with the options '--c-compiler'
and '--cxx-compiler' of "build.sh". 

Compiler compatibilities
------------------------

Tested on GCC 7.0 - 9.1.
Some performance regressions were noted with gcc 8.1.
Countermeasures have been introduced though some corner cases 
may still be a bit slower. Clang is unaffected by this regression.

Note that GCC <= 6 fails because it does not support correctly C++14 features.

Modules
-------

The core package supports to have extensions statically linked to the core.
They have to be put in extra/ and the cmake scripts will automatically link
to it. Check 'extra/demo/' for an example.

Usage policy
------------

Please cite the following articles for ARES2 and ARES3:

* Jasche, Kitaura, Wandelt, 2010, MNRAS, 406, 1 (arxiv 0911.2493)
* Jasche & Lavaux, 2015, MNRAS, 447, 2 (arxiv 1402.1763)
* Lavaux & Jasche, 2016, MNRAS, 455, 3 (arxiv 1509.05040)
 

BORG papers have a different listing.

For a full listing of publications from the Aquila consortium. Please check 
https://aquila-consortium.org/publications.html

Usage
-----


Output files
------------


Acknowledgements
----------------

This work has been funded by the following grants and institutions over the
years:

* the DFG cluster of excellenec "Origin and Structure of the Universe" 
  (http://www.universe-cluster.de).
* Institut Lagrange de Paris (grant ANR-10-LABX-63, http://ilp.upmc.fr) within 
  the context of the Idex SUPER subsidized by the French government through
  the Agence Nationale de la Recherche (ANR-11-IDEX-0004-02).
* BIG4 (ANR-16-CE23-0002) (https://big4.iap.fr)
* The "Programme National de Cosmologie et Galaxies" (PNCG, CNRS/INSU)
* Through the grant code ORIGIN, it has received support from
  the "Domaine d'Interet Majeur (DIM) Astrophysique et Conditions d'Apparitions
  de la Vie (ACAV)" from Ile-de-France region.
