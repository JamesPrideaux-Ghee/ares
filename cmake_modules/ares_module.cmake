macro(add_liblss_module module)
  set(_module_fname "${CMAKE_SOURCE_DIR}/extra/${module}/libLSS/${module}.cmake") 
  if (EXISTS ${_module_fname}) 
    set(BUILD_ARES_MODULE_${module})
    set(_ARES_current_parse_module ${module})
    # Add the libLSS in the module to the search path
    SET(ARES_INCLUDE_PATH ${ARES_INCLUDE_PATH} ${CMAKE_SOURCE_DIR}/extra/${module})
    include(${_module_fname})
  endif()
endmacro()

macro(add_liblss_test_module module)
  set(_module_fname "${CMAKE_SOURCE_DIR}/extra/${module}/libLSS/tests/tests.cmake") 
  if (EXISTS ${_module_fname}) 
    set(_ARES_current_parse_module ${module})
    include(${_module_fname})
  endif()
endmacro()

function(check_ares_module _my_var)
  set(${_my_var} TRUE PARENT_SCOPE)
  foreach(module IN LISTS ARGN)
    list(FIND ARES_MODULES ${module} _module_found)
    if(${_module_found} EQUAL -1)
      set(${_my_var} FALSE PARENT_SCOPE)
    endif()
  endforeach()
endfunction()

function(require_ares_module)
  check_ares_module(_result ${ARGV})
  if (NOT ${_result})
    cmessage(FATAL_ERROR "Module(s) ${ARGV} are necessary to build ${_ARES_current_parse_module}")
  endif()
endfunction()
