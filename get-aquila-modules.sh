#+
#   ARES/HADES/BORG Package -- -- ./get-aquila-modules.sh
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
#!/bin/sh

C_DEFAULT=$(echo -e "\033[0m")
C_WHITE=$(echo -e "\033[1m")
C_GREEN=$(echo -e "\033[92m")
C_RED=$(echo -e "\033[91m")
C_BG_RED=$(echo -e "\033[41m")
C_BG_GREEN=$(echo -e "\033[42m")

errormsg() {
  msg=$1
  echo -e "${C_BG_RED}${msg}${C_DEFAULT}"
}

trap "echo -e \"${C_DEFAULT}\"" SIGTERM EXIT 


print_help()
{
  cat<<EOF
This is the get-aquila-module helper script. It clones and updates the modules
common to the collaboration. By default it uses SSH protocol. We advise using
a SSH key to avoid being asked too much about your password.

--clone            Clone missing modules
--https USER       Use HTTPS protocol instead of SSH 
--pull             Pull all the modules
--push             Push all modules to origin (including root)
--help             This information message
--update-copyright Update copyright notices
--update-indent    Update indentation
--status           Show git status
--report           Create a git version report for reproductibility
--branch-set       Setup branches of default modules
--purge            Purge the content of modules (DANGEROUS)


Developer tools:
--tags             Tags all submodule with current version in ARES
EOF
}

# ===========================================
# get current branch in git repo
function parse_git_branch() {
     BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
     if [ ! "${BRANCH}" == "" ]
     then
          STAT=`parse_git_dirty`
          echo "(${BRANCH}${STAT})"
     else
          echo ""
     fi
}

# ===========================================
# get current status of git repo
function parse_git_dirty {
     status=`git status 2>&1 | tee`
     dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
     untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
     ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
     newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
     renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
     deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
     bits=''
     if [ "${renamed}" == "0" ]; then
          bits=">${bits}"
     fi
     if [ "${ahead}" == "0" ]; then
          bits="*${bits}"
     fi
     if [ "${newfile}" == "0" ]; then
          bits="+${bits}"
     fi
     if [ "${untracked}" == "0" ]; then
          bits="?${bits}"
     fi
     if [ "${deleted}" == "0" ]; then
          bits="x${bits}"
     fi
     if [ "${dirty}" == "0" ]; then
          bits="!${bits}"
     fi
     if [ ! "${bits}" == "" ]; then
          echo " ${bits}"
     else
          echo ""
     fi
}


# ===========================================
# Check whether current git is dirty
check_dirty()
{
  if test $force != 0; then
    return
  fi

  d=$1
  r=$(
    cd $d;
    git status --porcelain | grep '^ M'
  )
  if test x"$r" != x""; then
    echo "Module ${d} is not clean."
    exit 1
  fi
}

indent_out()
{
  echo "${C_RED} | "
  echo " ->"
  sed 's/\r/\n/' | sed "s/^/${C_RED}   |${C_DEFAULT}    /"
}
# ===========================================
# Send notice to user
#

warning_msg() {
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "This script can be run only by Aquila members."
echo "if your bitbucket login is not accredited the next operations will fail."
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
}

if ! command -v git 2>&1 > /dev/null; then
  echo "Git is required."
  exit 1
fi

# ===========================================
# Argument parsing
#
operation=''
prefix=git@bitbucket.org:
force=0
be_quiet=0
while test $# -gt 0; do
  key="$1"
  case $key in
  --clone)
     operation="clone"
     ;;
  --https)
     user="$2"
     prefix="https://${user}@bitbucket.org/"
     shift
     ;;
  --pull)
     operation="pull"
     ;;
  --push)
     operation="push"
     ;;
  --purge)
     operation="purge"
     ;;
  --update-copyright)
     operation="update_copyright"
     ;;
  --update-indent)
     operation="update_indent"
     ;;
  --force)
     force=1
     ;;
  --status)
     operation="status"
     ;;
  --report)
     operation="report"
     ;;
  --branch-set)
     operation="branch_set"
     ;;
  --tags)
     operation="tags"
     ;;
  -q)
     be_quiet=1
     ;;
  -h|--h|--he|--hel|--help)
     print_help
     exit 0
     ;;
  esac
  shift
done

if [ "$be_quiet" == "0" ]; then
  warning_msg
fi

# ===========================================

if test "x${operation}" = "x"; then
  errormsg "The operation to do is missing."
  print_help
  exit 1
fi

export prefix

# ===========================================
#
# Read the information about Aquila modules
submodules=""
while read; do
  if test "x${REPLY}" = "x"; then 
    continue
  fi
  set -- $REPLY;
  submodules="${submodules} $1,$2,$3"
done < .aquila-modules
# ===========================================

# ===========================================
# Execute the requested operation
#
case $operation in
tags)
   ares_version=$(git describe)
   release_msg="Release ${ares_version}"
   for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      branch=$3
      (cd extra/$sub; git tag -a -m "${release_msg}" ${ares_version}) | indent_out 
   done
   ;;
pull)
   echo "${C_WHITE}Pulling root...${C_DEFAULT}"
   ( git pull 2>&1 ) | indent_out
   for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      branch=$3
      echo "${C_WHITE}Pulling ${sub_git}${C_DEFAULT} into extra/${sub}"
      [ -e extra/$sub ] && (cd extra/$sub; git pull 2>&1 ) | indent_out
   done
   ;;
push)
   for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      branch=$3
      echo "${C_WHITE}Pushing ${sub_git}${C_DEFAULT}..."
      [ -e extra/$sub ] && (cd extra/$sub; git push origin ${branch} 2>&1 ) | indent_out
   done
   git push origin
   ;;
purge)
   for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      branch=$3
      [ -e extra/$sub ] && (
        cd extra/${sub};
        if [ -z "$(git status --porcelain)" ]; then
          # Working directory clean
	  cd ..
          echo "Purging ${sub}"
	  rm -fr ${sub}
        else
          echo "Not empty. Skipping"
        fi
      )
   done
   ;;
clone)
  for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      echo "Cloning ${sub_git} into extra/${sub}"
      (
        cd extra/
        if ! test -e ${sub}; then
          git clone  ${prefix}bayesian_lss_team/${sub_git}.git ${sub} 
        fi
      ) 2>&1 | indent_out
  done
  ;;
update_copyright)
  check_dirty .
  python3 build_tools/gather_sources.py
  for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      branch=$3
      echo "Updating ${sub}"
      check_dirty extra/${sub}
      [ -e extra/$sub ] && (cd extra/${sub};  python3 ../../build_tools/gather_sources.py)
  done
  ;;
update_indent)
  if ! command -v clang-format; then
    errormsg "Could not find clang-format in the PATH. It is required for reindentation."
    exit 1  
  fi
  CF=$(which clang-format)
  for i in $submodules; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      branch=$3
      if test -d extra/${sub}; then
        find extra/${sub} -name "*.[ch]pp" | xargs ${CF} -style=file -i  
      fi
  done
  ;;
branch_set)
  echo "Setting up branches for modules..."
  for i in ${submodules}; do
      IFS=","
      set -- $i;
      sub_git=$1
      sub=$2
      branch=$3
      echo "-- Switching ${sub} to ${branch}"
      [ -e extra/$sub ] && (
         cd extra/${sub};
	 if ! (git checkout ${branch} > /dev/null 2>&1); then
            errormsg "Problem changing branch on ${sub}"
            exit 1
         fi
      ) || exit 1
  done
  ;;
report)
  echo "GIT report"
  for i in . extra/*; do
    if test x$(basename $i) = xdemo; then
      continue
    fi
    (
      cd $i
      h=$(git rev-parse HEAD)
      mname=$(if [ "$i" == "." ]; then echo "root"; else echo $(basename $i); fi) 
      status=`git status 2>&1 | tee`
      dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
      code=""
      if [ "$dirty" == "0" ] ; then
        code='(?)'
      fi
      echo "- Module: ${mname}, Version${code}: ${h}"
    )
  done
  ;;
status)
  echo "Checking GIT status..."
  echo
  for i in . extra/*; do
    if test x$(basename $i) = xdemo; then
      continue
    fi
    (
      cd $i
      # https://stackoverflow.com/questions/1593051/how-to-programmatically-determine-the-current-checked-out-git-branch
      branch_name="$(git symbolic-ref HEAD 2>/dev/null)" || branch_name="(unnamed branch)"     # detached HEAD
      branch_name=${branch_name##refs/heads/}
      # --
      if [ "$i" == "." ]; then
	echo -e -n "Root tree\t (branch ${branch_name}) :"
      else
        echo -e -n "Module $(basename $i)\t(branch ${branch_name}) :"
      fi
      status=`git status 2>&1 | tee`
      dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
      untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
      ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
      newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
      renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
      deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
      code=$(
      [ "$dirty" == "0" ] && echo "  dirty"
      [ "$untracked" == "0" ] && echo "  untracked. Are you sure all files are under git supervision ?"
      [ "$ahead" == "0" ] && echo "  ahead. Run git push ?"
      [ "$newfile" == "0" ] && echo  "  newfile. Please git commit." 
      [ "$renamed" == "0" ] && echo  "  renamed. Please git commit."
      [ "$deleted" == "0" ] && echo  "  deleted. Please git commit.")
      if [ "x$code" == "x" ]; then
	 echo "$C_GREEN good. All clear. $C_DEFAULT "
      else
	 echo "$C_RED some things are not right. $C_DEFAULT "
	 echo "${code}"
         echo
      fi
    )
  done 
  ;;
esac
# ===========================================
